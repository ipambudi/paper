<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFinansialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('finansials', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ids');
            $table->string('user_id');
            $table->string('title');
            $table->string('finansial_account')->nullable();
            $table->string('finansial_name')->nullable();
            $table->decimal('amount',19,2)->nullable();
            $table->string('description')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('finansials');
    }
}
