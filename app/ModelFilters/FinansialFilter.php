<?php 

namespace App\ModelFilters;

use EloquentFilter\ModelFilter;

class FinansialFilter extends ModelFilter
{
    /**
    * Related Models that have ModelFilters as well as the method on the ModelFilter
    * As [relationMethod => [input_key1, input_key2]].
    *
    * @var array
    */
    public $relations = [];

    public function created_at($date){
        if($date){
            return $this->whereDate('created_at',$date);
        }
        return;
    }

    public function finansial_account($financial_account){
        if($financial_account){
            return $this->where('financial_account',$financial_account);
        }
        return;
    }

    public function finansial_name($financial_name){
        if($financial_name){
            return $this->where('financial_name',$financial_name);
        }
        return;
    }

    public function amount($amount){
        if($financial_name){
            return $this->where('amount',$amount);
        }
        return;
    }

    public function description($description){
        if($description){
            return $this->where('description',$description);
        }
        return;
    }

    public function title($title){
        if($title){
            return $this->where('title',$title);
        }
        return;
    }

    public function search($search){
        if($search){      
            return $this->where(function($q) use($search) {
                        $q->where('description','like','%'.$search.'%')
                        ->orWhere('finansial_account','like','%'.$search.'%')
                        ->orWhere('amount','like','%'.$search.'%')
                        ->orWhere('title','like','%'.$search.'%');
                    });
        }
        return;
    }
}
