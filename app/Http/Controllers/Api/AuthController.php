<?php

namespace App\Http\Controllers\Api;

//** model */
use App\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Controllers\Api\BaseController;
use JWTAuth;
use Validator;


class AuthController extends BaseController
{

    public function register(Request $request)
    {   
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255|unique:users',
            'name' => 'required|min:3',
            'password'=> 'required|min:6'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $user = new User();
        $user->name = $request->name;
        $user->user_id = 'temp';
        $user->email = $request->email;
        $user->status = 1;
        $user->password = bcrypt($request->password);
        $user->save();
        $user->user_id = date('mY').sprintf("%05d", $user->id);
        $user->save();
        $user = User::first();
        $token = JWTAuth::fromUser($user);

        return response()->json([
            'success' => true,
            'token' =>  $this->respondWithToken($token)
        ]);
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => ['required', 'email'],
            'password' => ['required']
        ]);

        if ($validator->fails()) { 
            return $this->resFailed('Login Failed',422,$validator->errors());
        }
        $credentials = $request->only('email', 'password');
        
        try {
            // attempt to verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt([
                'email'   =>$credentials['email'],
                'password'=>$credentials['password'],
                 'status' => 1 
            ])) {
                return $this->resFailed('Login Failed',401,"error' => 'Email atau Password Anda Salah");
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return $this->resFailed('Login Failed',500,"error' => 'could_not_create_token");
        }

        // all good so return the token
        return $this->respondWithToken($token);
    }

    public function logout(Request $request)
    {
        $this->validate($request, [
            'token' => 'required'
        ]);
 
        try {
            JWTAuth::invalidate($request->token);

            return $this->resSuccess("message' => 'User logged out successfully");

        } catch (JWTException $exception) {
            
            return $this->resFailed('Logout Failed',500,"Sorry, the user cannot be logged out");
        }
    }

    public function getAuthUser(){
        $data = [
                'name'       => auth('api')->user()->name,
                'user_id'   => auth('api')->user()->user_id,
                'email'     => auth('api')->user()->email,
        ];
        return $this->resSuccess($data);
    }

}
