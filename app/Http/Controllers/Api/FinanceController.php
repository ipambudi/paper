<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\finansial as arrayFinansial;
use App\Http\Resources\finansialcoll;
use Illuminate\Support\Facades\DB;
use App\finansial;
use Validator;

class FinanceController extends BaseController
{
    private $perPage = 10;

    public function index(Request $request){
        $data = finansial::where('user_id',auth('api')->user()->user_id)->filter($request->all())->paginate($this->perPage);
        $data= new finansialcoll($data);
        return $this->resSuccess($data);
        
    }

    public function store(Request $request){

        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'amount' => "required|regex:/^\d+(\.\d{1,2})?$/",
        ]);
        if ($validator->fails()) {
            return $this->resFailed('Store Failed',422,$validator->errors());
        }

        $ids= $request->id;
        if(!$ids||!finansial::where('ids',$ids)->where('user_id',auth('api')->user()->user_id)->latest()->withTrashed()->first()){ 
           
            $records = finansial::where('user_id',auth('api')->user()->user_id)->latest()->withTrashed()->first();
            $ids = sprintf("%05d", 1);

            if(!empty($records->ids)){
                $ids = sprintf("%05d", $records->ids+1);
            }
        }
       
        $data = finansial::updateOrCreate(
            ['ids'      =>$ids,
            'user_id'   =>auth('api')->user()->user_id],

            ['title'              => $request->title,
             'finansial_account'  => $request->finansial_account,
             'finansial_name'     => $request->finansial_name,
             'description'        => $request->description,
             'amount'            => $request->amount]
        );

        return $this->resSuccess("message' => 'successfully");
    }

    public function softdelete(Request $request){

        $validator = Validator::make($request->all(), [
            'id' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->resFailed('Delete Failed',422,$validator->errors());
        }
       
        $rec = finansial::where('ids',$request->id)->first();
        if(empty($rec->id)){
            return $this->resFailed('Delete Failed',422,"Data not found");
        }

        $rec->delete();      
        
        return $this->resSuccess("message' => 'successfully");

    }

    public function detail($id){
        $data = finansial::where('ids',$id)->first();

        if(empty($data->id)){
            return $this->resFailed('Detail Failed',422,"Data not found");
        }
        $data= new arrayFinansial($data);
        return $this->resSuccess($data);
    }

    public function summary($group){
        $data = [];
        if($group=='month'){
            $data = DB::table('finansials')->select(  DB::raw(' sum(amount) total, MONTH(created_at) month'))
            ->groupby('month')
            ->get();
        }

        if($group=='day'){
            $data = DB::table('finansials')->select(  DB::raw(' sum(amount) total, DATE(created_at) day'))
            ->groupby('day')
            ->get();
        }
       
        return $this->resSuccess($data);
    }



}
