<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BaseController extends Controller
{
    public function resSuccess($data, $status=200, $message= 'Success')
    {
        $respon = [
            'code'      => $status,
            'data'      => $data,
            'message'   => $message
        ];
        
        return response()->json($respon,$status);
    }
    
    public function resFailed($errortype, $status=422, $errorDetail= [])
    {
        $respon = [
            'code'      => $status,
            'error'     => $errortype
        ];

        if(!empty($errorDetail)){
            $respon['errorDetails'] =$errorDetail; 
        }
        
        return response()->json($respon,$status);
    }
    
    public function respondWithToken($token, $status=200)
    {
        $respon = [
            'code'          => $status,
            'access_token'  => $token,
            'token_type'    => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60,
        ];
        return response()->json($respon,$status);
    }
}
