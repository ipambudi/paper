<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class finansial extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->ids,
            'title' => $this->title,
            'finansial_account' => $this->finansial_account,
            'finansial_name' => $this->finansial_name,
            'amount' => $this->amount,
            'description' => $this->description,
            'date' => $this->created_at->format('Y-m-d H:i:s')
        ];
    }
}
