<?php

namespace App;

use EloquentFilter\Filterable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class finansial extends Model
{
    use Filterable;
    use SoftDeletes;
    protected $guarded = [];
}
