<?php

use Illuminate\Http\Request;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\FinanceController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });



Route::post('login',     [AuthController::class, 'login']);
Route::post('register',  [AuthController::class, 'register']);
 
Route::group(['middleware' => 'auth.jwt'], function () {
    Route::get('logout',[AuthController::class, 'logout']);
    Route::get('user',  [AuthController::class, 'getAuthUser']);
});

Route::group(['middleware' => 'auth.jwt' , 'prefix' => 'finance/'], function () {
    Route::get('list',    [FinanceController::class, 'index']);
    Route::post('store',  [FinanceController::class, 'store']);
    Route::get('delete',  [FinanceController::class, 'softdelete']);
    Route::get('detail/{id}',  [FinanceController::class, 'detail']);
    Route::get('summary/{id}',  [FinanceController::class, 'summary']);
});